# Spring Boot REST API One to Many

In the repository, we show how to expose CRUD RestAPIs `Post/Get/Put/Delete` 
to interact with Hibernate Spring JPA One-to-Many association models using SpringBoot and PostgreSQL database.

We exposes RestAPIs for Post/Get/Put/Delete Students & Assignments:

– Student ->

-    `@GetMapping("/api/students"):` get all Students
-    `@GetMapping("/api/students/{id}"):` get a Student by ID
-    `@PostMapping("/api/students"):` post a Student
-    `@PutMapping("/api/students/{id}"):` update a Student
-    `@DeleteMapping("/api/students/{id}"):` delete a Student

– Assignments ->

-    `@GetMapping("/students/{studentId}/assignments"):` get a Assignment by Student’s ID
-    `@PostMapping("/students/{studentId}/assignments"):` add an Assignment
-    `@PutMapping("/students/{studentId}/assignments/{assignmentId}"):` update an Assignment
-    `@DeleteMapping("/students/{studentId}/assignments/{assignmentId}"):` delete an Assignment by ID