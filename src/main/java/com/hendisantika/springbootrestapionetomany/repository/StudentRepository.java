package com.hendisantika.springbootrestapionetomany.repository;

import com.hendisantika.springbootrestapionetomany.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-rest-api-one-to-many
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-12
 * Time: 22:28
 */
public interface StudentRepository extends JpaRepository<Student, Long> {
}
